﻿using CMS.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;
using Microsoft.CodeAnalysis.Operations;

namespace CMS.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginDTO loginDTO)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(loginDTO);
            string errorMessage = string.Empty;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5213/api/Customer/login");
                request.Content = new StringContent(json, Encoding.UTF8);
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.Send(request);
                var content = response.Content.ReadAsStringAsync();
                var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

                if (apiResultModel != null)
                {
                    if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                    {
                        HttpContext.Session.SetString("email", loginDTO.email);
                        HttpContext.Session.SetString("password", loginDTO.password);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        errorMessage = "Email atau Password salah!";
                    }
                }
            }
            catch(Exception ex)
            {
                errorMessage = "Error";
            }

            ViewBag.Error = errorMessage;
            return View();
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("email");
            return View("Login");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(CustomerDTOModel model)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(model);
            string errorMessage = string.Empty;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5213/api/Customer");
                request.Content = new StringContent(json, Encoding.UTF8);
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.Send(request);
                var content = response.Content.ReadAsStringAsync();
                var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

                if (apiResultModel != null)
                {
                    if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                    {
                        return View("Login");
                    }
                    else
                    {
                        errorMessage = "Error";
                    }
                    errorMessage = "Error";
                }
            }
            catch (Exception ex)
            {
                errorMessage = "Error";
            }

            ViewBag.Error = errorMessage;
            return View("Login");
        }

        [HttpPost]
        public IActionResult Update(CustomerDTOModel model)
        {
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(model);
            string errorMessage = string.Empty;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5213/api/Customer/update");
                request.Content = new StringContent(json, Encoding.UTF8);
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = client.Send(request);
                var content = response.Content.ReadAsStringAsync();
                var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

                if (apiResultModel != null)
                {
                    var result = JsonConvert.DeserializeObject<CustomerDTOModel>(apiResultModel.data.ToString());
                    if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                    {
                        HttpContext.Session.Remove("email");
                        HttpContext.Session.SetString("email", model.Email);
                        HttpContext.Session.SetString("password", model.Password);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        errorMessage = "Error";
                    }
                    errorMessage = "Error";
                }
            }
            catch (Exception ex)
            {
                errorMessage = "Error";
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
