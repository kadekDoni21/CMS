﻿using CMS.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace CMS.Controllers
{
    public class InsuranceController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("email") == null)
            {
                return RedirectToAction("Login", "Account");
            }
            
            var customer = Data.Manage.Customer.GetByEmail(HttpContext.Session.GetString("email"));
            var customerInsurance = Data.Manage.Insurance.GetByCustomerID(customer.ID);
            if (customerInsurance.Count > 0)
            {
                return View(customerInsurance);
            }
            return View();
        }

        [HttpGet]
        public IActionResult Add()
        {
            InsuranceAddViewModel viewModel = new InsuranceAddViewModel();
            var insurance = Data.Manage.Insurance.GetAllInsurance();
            viewModel.insurances = insurance;

            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Add(InsuranceAddViewModel model)
        {
            var customer = Data.Manage.Customer.GetByEmail(HttpContext.Session.GetString("email"));
            var client = new HttpClient();

            var reqModel = new AddCustomerInsuranceRequest
            {
                customerID = customer.ID,
                InsurancesID = model.insuranceID
            };


            var json = JsonConvert.SerializeObject(reqModel);
            string errorMessage = string.Empty;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:5176/api/Insurance/add-customer-insurance");
                request.Content = new StringContent(json, Encoding.UTF8);
                request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var response = client.Send(request);
                var content = response.Content.ReadAsStringAsync();
                var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

                if (apiResultModel != null)
                {
                    if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                    {
                        return RedirectToAction("Index", "Insurance");
                    }
                    else
                    {
                        errorMessage = "Failed to add Customer Insurance";
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = "Error";
            }
            return RedirectToAction("Index", "Insurance");
        }

        public void PopulateInsurance()
        {
            var insurance = Data.Manage.Insurance.GetAllInsurance();

            List<SelectListItem> listInsurance = new List<SelectListItem>();

            foreach (var data in insurance)
            {
                listInsurance.Add(new SelectListItem { Text = data.Name, Value = data.ID.ToString() });
            }
            ViewBag.Insurance = listInsurance;
        }

        [HttpGet]
        public IActionResult Detail(Guid id)
        {
            var data = Data.Manage.Insurance.GetDetailCustomerInsurance(id);

            return View(data);
        }
    }
}
