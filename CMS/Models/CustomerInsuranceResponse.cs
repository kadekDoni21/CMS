﻿using CMS.Data.Manage;

namespace CMS.Models
{
    public class CustomerInsuranceResponse
    {
        public CustomerInsurance CustomerInsurance { get; set; }
        public Insurance Insurance { get; set; }
    }
}
