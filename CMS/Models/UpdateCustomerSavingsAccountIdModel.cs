﻿namespace CMS.Models
{
    public class UpdateCustomerSavingsAccountIdModel
    {
        public Guid CustomerID { get; set; }
        public Guid SavingsAccountID { get; set; }
    }
}
