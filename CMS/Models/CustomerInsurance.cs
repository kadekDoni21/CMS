﻿namespace CMS.Models
{
    public class CustomerInsurance
    {
        public Guid ID { get; set; }
        public Guid SavingsAccountID { get; set; }
        public Guid InsuranceID { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
