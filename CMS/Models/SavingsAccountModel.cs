﻿namespace CMS.Models
{
    public class SavingsAccountModel
    {
        public Guid CustomerID { get; set; }
        public Guid ID { get; set; }
        public Guid SavingsTypeId { get; set; }
        public string RekeningNumber { get; set; }
        public int Pin { get; set; }
        public long Balance { get; set; }
        public SavingsTypeModel SavingsType { get; set; }
    }
}
