﻿using CMS.Models;
using Newtonsoft.Json;
using System.Net;

namespace CMS.Data.Manage
{
    public class SavingsAccounts
    {
        public static SavingsAccountResponse GetByID (Guid ID)
        {
            var responseModel = new SavingsAccountResponse();
            HttpClient client = new HttpClient();
            string uri = "http://localhost:5176/api/Savings/get-savings-account/" + ID.ToString();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var test = client.Send(request);

            var content = test.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    responseModel = JsonConvert.DeserializeObject<SavingsAccountResponse>(apiResultModel.data.ToString());
                    return responseModel;
                }
            }
            return null;
        }
    }
}
