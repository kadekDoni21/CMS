﻿using CMS.Models;
using Newtonsoft.Json;
using System.Net;

namespace CMS.Data.Manage
{
    public class Insurance
    {
        public static List<CustomerInsuranceResponse> GetByCustomerID(Guid id)
        {
            List<CustomerInsuranceResponse> result = new List<CustomerInsuranceResponse>();
            HttpClient client = new HttpClient();
            string uri = "http://localhost:5176/api/Insurance/get-customer-insurance/" + id.ToString();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var test = client.Send(request);

            var content = test.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    result = JsonConvert.DeserializeObject<List<CustomerInsuranceResponse>>(apiResultModel.data.ToString());
                    return result;
                }
            }
            return result;
        }

        public static List<Models.Insurance> GetAllInsurance()
        {
            List<Models.Insurance> result = new List<Models.Insurance>();
            HttpClient client = new HttpClient();
            string uri = "http://localhost:5176/api/Insurance/get-insurance";
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var test = client.Send(request);

            var content = test.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    result = JsonConvert.DeserializeObject<List<Models.Insurance>>(apiResultModel.data.ToString());
                    return result;
                }
            }
            return result;
        }

        public static DetailCustomerInsuranceViewModel GetDetailCustomerInsurance(Guid Id)
        {
            DetailCustomerInsuranceViewModel result = new DetailCustomerInsuranceViewModel();
            HttpClient client = new HttpClient();
            string uri = "http://localhost:5176/api/Insurance/get-customer-insurance-by-id/" + Id.ToString();
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var test = client.Send(request);

            var content = test.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    result = JsonConvert.DeserializeObject<DetailCustomerInsuranceViewModel>(apiResultModel.data.ToString());
                    return result;
                }
            }
            return result;
        }
    }
}
