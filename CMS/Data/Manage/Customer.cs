﻿using CMS.Models;
using Newtonsoft.Json;
using System.Net;

namespace CMS.Data.Manage
{
    public class Customer
    {
        public static CustomerDTOModel GetByEmail(string email)
        {
            var responseModel = new CustomerDTOModel();
            HttpClient client = new HttpClient();
            string uri = "http://localhost:5213/api/Customer/" + email;
            var request = new HttpRequestMessage(HttpMethod.Get, uri);
            var test = client.Send(request);

            var content = test.Content.ReadAsStringAsync();
            var apiResultModel = JsonConvert.DeserializeObject<ApiResultModel>(content.Result);

            if (apiResultModel != null)
            {
                responseModel = JsonConvert.DeserializeObject<CustomerDTOModel>(apiResultModel.data.ToString());
                if (apiResultModel.statusCode == (int)HttpStatusCode.OK)
                {
                    return responseModel;
                }
            }
            return null;
        }
    }
}
